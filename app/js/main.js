$(document).ready(function(){
    $(".settings-option-btn").on("click", function(){
        var $settingOptions = $('.settings-option-wrapper');
        if($settingOptions.hasClass("hide")) {
            $settingOptions.removeClass('hide');
        }
        else {
            $settingOptions.addClass('hide');
        }
    });
    $('.settings-option-header').on('click', function() {
        var $parent = $(this).parent();
        if($parent.hasClass("active")) {
            $parent.removeClass('active');
            $parent.find(".settings-option-body").addClass("hide");
        } else {
            $parent.addClass('active');
            $parent.find(".settings-option-body").removeClass("hide");
        }

    });

    $(".menu-option-btn").on("click", function(){
        var $settingOptions = $('.menu-option-wrapper');
        if($settingOptions.hasClass("hide")) {
            $settingOptions.removeClass('hide');
        }
        else {
            $settingOptions.addClass('hide');
        }
    });

    $('.navigation-item-header').on('click', function() {
        var $parent = $(this).parent();
        if($parent.hasClass("active")) {
            $parent.removeClass('active');
            $parent.find(".navigation-item-body").addClass("hide");
        } else {
            $parent.addClass('active');
            $parent.find(".navigation-item-body").removeClass("hide");
        }
    });

    $('.tab-headers').on('click', function(e) {
        var $this =  $(this);
        var tabId = $this.attr('data-tab');

        $(".active").removeClass("active");
        $this.addClass('active');
        $("."+tabId).addClass('active');

    });
});