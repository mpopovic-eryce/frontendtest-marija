// *****************************************
//
//   Gulpfile
//
// ***************************************************************
//
// Available tasks:
//   `gulp compile:sass`
//   `gulp browserSync`
//   `gulp watch:`
//   `gulp minify:css`
//   `gulp minify:js`
//   `gulp minify:images`
//   `gulp copy:html`
//   `gulp build`
//
// ***************************************************************
// ---------------------------------------------------------------
//   Modules
// ---------------------------------------------------------------
//   gulp                 : The streaming build system
//   gulp-sass            : Compile Sass
//   gulp-cssnano         : Minify CSS files
//   browserSync          : Start a static server
//   sourcemaps           : Create sourcemap files
//   gulp-uglify          : Minify JavaScript files
//   gulp-imagemin        : Minify PNG, JPEG, GIF and SVG images
//
// ---------------------------------------------------------------

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    cssnano = require('gulp-cssnano'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create();

var sassSource = 'app/scss/**/*.scss',
    jsSource = 'app/js/*.js';

var sassDestination = 'app/css';

// ---------------------------------------------------------------
//  Task: Init Browser Sync
// ---------------------------------------------------------------
    gulp.task('browserSync', function() {
        browserSync.init({
            server: {
                baseDir: 'app'
            },
        })
    });


// ---------------------------------------------------------------
//  Task: Compile Sass
// ---------------------------------------------------------------
    gulp.task('compile:sass', function () {
        return gulp.src(sassSource)
            .pipe(sass().on('error', function(err) {
                console.error(err.message);
                browserSync.notify(err.message, 3000);
                this.emit('end');
            }))
            .pipe(gulp.dest(sassDestination))
            .pipe(browserSync.reload({
                stream: true
            }))
    });


// ---------------------------------------------------------------
//   Task: Watch
// ---------------------------------------------------------------
    gulp.task('watch', ['browserSync', 'compile:sass'], function () {
        gulp.watch(sassSource, ['compile:sass']);
        gulp.watch("app/*.html").on('change', browserSync.reload);
    });


// ---------------------------------------------------------------
//  Task: Minify: CSS
// ---------------------------------------------------------------
    gulp.task('minify:css', function () {
        return gulp.src('app/css/main.css')
            .pipe(sourcemaps.init())
            .pipe(cssnano())
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest('./app/dist/css'));
    });


// ---------------------------------------------------------------
//  Task: Minify: JS
// ---------------------------------------------------------------
    gulp.task('minify:js', function () {
        return gulp.src(jsSource)
            .pipe(uglify())
            .pipe(gulp.dest('./app/dist/js'));
    });


// ---------------------------------------------------------------
//  Task: Minify: Images
// ---------------------------------------------------------------
    gulp.task('minify:images', function () {
        return gulp.src('app/images/*')
            .pipe(imagemin(
                imagemin.jpegtran({progressive: true}),
                imagemin.optipng({optimizationLevel: 5})
            ))
            .pipe(gulp.dest('./app/dist/images'));
    });

// ---------------------------------------------------------------
//  Task: Copy HTML to dist
// ---------------------------------------------------------------
    gulp.task('copy:html', function(){
        return gulp.src('app/index.html')
            .pipe(gulp.dest('app/dist'))
    });


// ---------------------------------------------------------------
//  Task: Build
// ---------------------------------------------------------------
    gulp.task('build', ['copy:html','minify:css', 'minify:js', 'minify:images']);